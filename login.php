<!doctype html>
<html lang="es">
    <head>
        <title>Iniciar sesión</title>
        <?php include("./header.php"); ?>
    </head>

    <body>
        <?php include("./nav.php"); ?>
    </body>

    <div class="container">
        <?php include("./user/login.php"); ?>
    </div>

</html>