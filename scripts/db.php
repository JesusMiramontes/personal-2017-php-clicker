<?php

# Si no existe, crea una sesión.
if(!isset($_SESSION)){
    session_start();
}

# Almacena la conexión abierta a la base de datos.
global $connection;

# Crea la conexión.
$connection = mysqli_connect("127.0.0.1", "root", "", "crudapp");
