Clicker
===================


----------


**Esquema**
-------------

 - **CSS:** Hoja de estilo.
 - **GAME:** Script del juego y configuración del sprite.
 - **SCRIPTS:** Controladores.
 - **USER:** Formularios.
 - **RAÍZ:** Vistas, header, y menú de navegación.

----------

**Configuración**
-------------

**Base de datos (MYSQL)**

> Configurar base de datos en: ***/scripts/db.php***

    CREATE TABLE `users` (
      `id` int(11) NOT NULL,
      `username` varchar(255) NOT NULL,
      `email` varchar(255) NOT NULL,
      `password` varchar(255) NOT NULL,
      `activation_key` varchar(255) NOT NULL,
      `is_active` enum('0','1') NOT NULL,
      `date_time` date NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    
    ALTER TABLE `users`
      ADD PRIMARY KEY (`id`);
    
    ALTER TABLE `users`
      MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;

**Mailer**

> Se utiliza SwiftMailer como Mailer. Puede cambiarse la configuración de correo desde: ***/scripts/insert.php*** y ***/scripts/forgot.php*** 

*Nota': Ya se incluye la configuración de una cuenta de gmail creada solo con fines de esta actividad.* 