
$counter = 0;
$point = false;
$highscore = 0;

$(function(){
    $point = false;

    $("#sprite").on
    (
        {
            mouseover:function()
            {
                $(this).css({
                    left:(Math.random()*500)+"px",
                    top:(Math.random()*500)+"px",
                });
                $point = false;
            },

            click:function()
            {
                $counter++
                $(this).text($counter);
                console.log($counter);

                $(this).css({
                    left:(Math.random()*800)+"px",
                    top:(Math.random()*800)+"px",
                });

                $point = true;
                if($highscore < $counter)
                {
                    $highscore = $counter;
                }
            },

            mouseleave:function()
            {
                //$(this).text("Click me");

                $(this).css({
                    left:(Math.random()*800)+"px",
                    top:(Math.random()*800)+"px",
                });
                $point = false;
            }
        }
    );

    $("#exit").on
    (
        {
            mouseover:function()
            {
                $(this).css({
                    left:(Math.random()*800)+"px",
                    top:(Math.random()*800)+"px",
                });
            },

            click:function()
            {
                console.log("Sent to save_score.php");
                if($highscore > 0)
                    {
                        $.post('./scripts/save_score.php', {score: $highscore}, function(){console.log("success");});
                    }
            },

            mouseleave:function()
            {
                $(this).css({
                    left:(Math.random()*800)+"px",
                    top:(Math.random()*800)+"px",
                });
            }
        }
    );

    $("div").on({
        click:function(){
            if(!$point)
            {
                $counter = 0;
                $("#sprite").text($counter);
                console.log("lost");
            }
        }
    });

});
